#!/usr/bin/env bash

set -e
WORKING_DIR=$(dirname "$0")
source $WORKING_DIR/functions.sh
INSTALL_REPO=https://tanzu-e2e-install.s3.amazonaws.com/deploy.tar

if [[ -z $WORKING_DIR/values.yaml ]]; then
    echo "Missing values.yaml, setup your values.yaml file and run again"
fi

today=`date '+%Y_%m_%d__%H_%M_%S'`;
log_file=$WORKING_DIR/tanzu-e2e-${today}.log
touch $log_file

KAPP_VERSION=v0.35.0
YTT_VERSION=v0.31.0
HELM_VERSION=helm-v3.5.2-linux-amd64
KUBECTL_VERSION=v1.20.0
GIT_VERSION=git-2.9.5
KBLD_VERSION=v0.29.0
POSTGRESQL_VERSION=v1.0.0

#ytt
wget -q -O ytt "https://github.com/vmware-tanzu/carvel-ytt/releases/download/${YTT_VERSION}/ytt-linux-amd64" 
chmod +x ytt 
sudo mv ytt /usr/local/bin

#kapp
wget -q -O kapp "https://github.com/vmware-tanzu/carvel-kapp/releases/download/${KAPP_VERSION}/kapp-linux-amd64" 
chmod +x kapp 
sudo mv kapp /usr/local/bin

#helm
wget -q -o get_helm.sh https://get.helm.sh/${HELM_VERSION}.tar.gz
tar -xvf ${HELM_VERSION}.tar.gz
chmod +x linux-amd64/helm
sudo mv linux-amd64/helm /usr/local/bin

#kbld
wget -q -O kbld https://github.com/vmware-tanzu/carvel-kbld/releases/download/${KBLD_VERSION}/kbld-linux-amd64 
chmod +x kbld 
sudo mv kbld /usr/local/bin

#kubectl
wget -q -O kubectl https://dl.k8s.io/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl 
chmod +x kubectl 
sudo mv kubectl /usr/local/bin

#kp
wget -q -O kp https://github.com/vmware-tanzu/kpack-cli/releases/download/v0.2.0/kp-linux-0.2.0 
chmod +x kp 
sudo mv kp /usr/local/bin

#yq
wget -q -O yq https://github.com/mikefarah/yq/releases/download/v4.5.0/yq_linux_amd64 
chmod +x yq
sudo mv yq /usr/local/bin

#install fly
curl -sSL "https://github.com/concourse/concourse/releases/download/v6.7.3/fly-6.7.3-linux-amd64.tgz" |sudo tar -C /usr/local/bin/ --no-same-owner -xzv fly 
 
curl -sL -o deploy.tar $INSTALL_REPO -vL 
tar xvf deploy.tar 

#grab values that are needed for script
user=$(yq e '.common.dockerUser' values.yaml )
password=$(yq e '.common.dockerPassword' values.yaml )
domain=$(yq e '.ingress.domain' values.yaml )
registry="harbor.$domain"
email=$(yq e '.common.dockerEmail' values.yaml )
secret_name=$(yq e '.common.imagePullSecret' values.yaml )
tanzu_user=$(yq e '.tanzu.user' values.yaml )
tanzu_password=$(yq e '.tanzu.password' values.yaml )
harbor_password=$(yq e '.harbor.adminPassword' values.yaml )

#metrics
kubectl apply -f metrics/metrics.yaml

#contour
curl -sL  https://projectcontour.io/quickstart/contour.yaml > contour.yaml
ytt --ignore-unknown-comments -f contour.yaml -f $WORKING_DIR/values.yaml -f $WORKING_DIR/common/pull-secret.yaml -f $WORKING_DIR/common/lb-external-traffic.yaml | kubectl apply -f- >> $log_file
create_docker_secret "projectcontour" $user $password $email $secret_name >> $log_file
sleep 10
kubectl get services envoy -n projectcontour 
read -p "create wildcard entry for your lb and press enter to proceed..."

#certgen
$WORKING_DIR/certgen/install.sh values.yaml >> $log_file
create_docker_secret "cert-manager" $user $password $email $secret_name >> $log_file

echo "Cert-manager installed and cluster issuer created..."

#harbor
if [ -z "$skip_harbor" ]
then

  kubectl create ns harbor -o yaml --dry-run=client| kubectl apply -f-  >> $log_file
  create_docker_secret "harbor" $user $password $email $secret_name  >> $log_file
  $WORKING_DIR/harbor/install-harbor.sh values.yaml  >> $log_file
fi

echo "Harbor installed..."
echo "Navigate to $registry in your browser and login with the admin user (password in values.yaml)"
echo "Create public projects in harbor named tanzu-build-service, tanzu-sql, and spring-music"
read -p "Hit enter to proceed..."

##fluentbit
kubectl create ns fluentbit -o yaml --dry-run=client| kubectl apply -f-  >> $log_file
create_docker_secret "fluentbit" $user $password $email $secret_name  >> $log_file
$WORKING_DIR/fluentbit/install-fluentbit.sh values.yaml  >> $log_file
echo "Fluentbit installed..."

#build service
tar xvf build-service-1.1.4.tar -C /tmp  >> $log_file
docker login registry.pivotal.io -u $tanzu_user -p $tanzu_password  >> $log_file
docker login $registry/tanzu-build-service/build-service -u admin -p $harbor_password  >> $log_file

kbld relocate -f /tmp/images.lock --lock-output /tmp/images-relocated.lock --repository "$registry/tanzu-build-service/build-service"  >> $log_file
install_tbs "$registry/tanzu-build-service/build-service" "admin" $harbor_password  >> $log_file
sleep 20
kp import -f descriptor-100.0.55.yaml  >> $log_file
sleep 5
tbs_harbor_creds $registry "admin"
echo "Tanzu Build Service installed..."

##prometheus
kubectl create ns prometheus -o yaml --dry-run=client| kubectl apply -f-  >> $log_file
create_docker_secret "prometheus" $user $password $email $secret_name  >> $log_file
$WORKING_DIR/prometheus/install-prometheus.sh values.yaml  >> $log_file

echo "Prometheus installed..."

##grafana
kubectl create ns grafana -o yaml --dry-run=client| kubectl apply -f-  >> $log_file
create_docker_secret "grafana" $user $password $email $secret_name  >> $log_file
$WORKING_DIR/grafana/install-grafana.sh values.yaml  >> $log_file

echo grafana admin password: $(kubectl get secret -n grafana grafana-admin -o jsonpath="{.data.GF_SECURITY_ADMIN_PASSWORD}" | base64 --decode ; echo)
echo "Grafana installed..."

##elastic 
kubectl create ns elasticsearch -o yaml --dry-run=client| kubectl apply -f-  >> $log_file
create_docker_secret "elasticsearch" $user $password $email $secret_name  >> $log_file
$WORKING_DIR/elasticsearch/install-elasticsearch.sh values.yaml  >> $log_file

echo "Elasticsearch installed..."

##kibana
kubectl create ns kibana -o yaml --dry-run=client| kubectl apply -f-  >> $log_file
create_docker_secret "kibana" $user $password $email $secret_name  >> $log_file
$WORKING_DIR/kibana/install-kibana.sh values.yaml  >> $log_file

echo "Kibana installed..."

##sonarqube
kubectl create ns sonarqube -o yaml --dry-run=client| kubectl apply -f-  >> $log_file
create_docker_secret "sonarqube" $user $password $email $secret_name  >> $log_file
$WORKING_DIR/sonarqube/install-sonarqube.sh values.yaml  >> $log_file

echo "Sonarqube installed..."

#postgresql
tar xvf postgres-for-kubernetes-${POSTGRESQL_VERSION}.tar.gz
$WORKING_DIR/postgresql/install.sh values.yaml $registry >> $log_file


#kubeapps
kubectl create ns kubeapps -o yaml --dry-run=client| kubectl apply -f-  >> $log_file
create_docker_secret "kubeapps" $user $password $email $secret_name  >> $log_file
$WORKING_DIR/kubeapps/install-kubeapps.sh values.yaml  >> $log_file

echo "Kubeapps installed..."

sleep 10

kubectl get -n kubeapps secret $(kubectl get serviceaccount -n kubeapps kubeapps-operator -o jsonpath='{range .secrets[*]}{.name}{"\n"}{end}' | grep kubeapps-operator-token) -o jsonpath='{.data.token}' -o go-template='{{.data.token | base64decode}}' && echo
echo "kubeapps login token"






