#!/bin/bash

API_URL=$(kubectl cluster-info | grep -E 'Kubernetes master|Kubernetes control plane' | awk '/http/ {print $NF}')
NS=$(kubectl get secret -A | grep default-token | head -n 1 | tr -s ' ' |cut -d' ' -f1)
SECRET=$(kubectl get secret -A | grep default-token | head -n 1 | tr -s ' ' |cut -d' ' -f2)
CA_CERTIFICATE=$(kubectl get secret $SECRET -n $NS -o jsonpath="{['data']['ca\.crt']}" | base64 --decode)
kubectl apply -f gitlab/gitlab.yaml
TOKEN=$(kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep gitlab | awk '{print $1}'))

echo API URL: "$API_URL"
echo CA Certificate:
echo "$CA_CERTIFICATE"
echo Service Token:
echo "$SERVICE_TOKEN"
echo "$TOKEN"

