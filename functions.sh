create_docker_secret() {
  namespace=$1
  user=$2
  pass=$3
  email=$4
  name=$5
  kubectl create secret docker-registry $name --dry-run=client --docker-username=$user --docker-password=$pass --docker-email=$email -n $namespace -o yaml | kubectl apply -f -
}

install_tbs() {
  REG=$1
  USER=$2
  PASS=$3
  
  ytt -f /tmp/values.yaml \
      -f /tmp/manifests/ \
      -v docker_repository="${REG}" \
      -v docker_username=${USER} \
      -v docker_password=${PASS} \
      | kbld -f /tmp/images-relocated.lock -f- \
      | kapp deploy -a tanzu-build-service -f- -y

}

tbs_harbor_creds() {
  REG=$1
  USER=$2

  echo "Please enter the password for your harbor registry admin user (in values.yaml)"
  kp secret create harbor \
  --registry $REG \
  --registry-user $USER
}

